{
  description = "jugendhacker NixOS configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/release-24.11";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    fdroid-news = {
      url = "git+https://git.sr.ht/~j-r/fdroid-news?ref=main";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-minecraft = {
      url = "github:Infinidoge/nix-minecraft";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ self, nixpkgs, nixpkgs-unstable, fdroid-news, sops-nix, nixos-generators, nix-minecraft, ... }:
    let
      inherit (nixpkgs) lib;

      hosts = import ./hosts.nix;
      getHostAddr = name:
        let
          host = hosts."${name}";
        in
        if host ? ipv4 then host.ipv4
        else if host ? ipv6 then host.ipv6
        else throw "Host ${name} does not have an IPv4 nor an IPv6";
      getUserByHost = name:
        let
          host = hosts."${name}";
        in
        if host ? user then host.user
        else "root";
    in
    {
      overlay = import ./overlays;
      nixosModules = {
        gotosocial = ./modules/gotosocial.nix;
        slidge = ./modules/slidge.nix;
        tgtg = ./modules/tgtg.nix;
      };

      formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixpkgs-fmt;

      legacyPackages = lib.attrsets.mapAttrs (system: pkgs: pkgs.appendOverlays [ self.overlay ]) nixpkgs.legacyPackages;
      packages = lib.attrsets.mapAttrs
        (system: pkgs:
          builtins.foldl'
            (result: name:
              let
                host = getHostAddr name;
                userName = getUserByHost name;
                target = ''"${userName}"@"${host}"'';
                rebuildArg = "--flake ${self}#${name}";
              in
              result // {
                "${name}-rebuild" = pkgs.writeScriptBin "${name}-rebuild" ''
                  #!${pkgs.runtimeShell} -ex
                  [[ $(ssh ${target} cat /etc/hostname) == ${name} ]]
                  nix copy --to ssh://${target} ${self}
                  ssh ${target} sudo nixos-rebuild ${rebuildArg} "$@"
                '';
                "${name}-rebuild-local" = pkgs.writeScriptBin "${name}-rebuild-local" ''
                  #!${pkgs.runtimeShell} -ex
                  [[ $(ssh ${target} cat /etc/hostname) == ${name} ]]
                  ${pkgs.nixos-rebuild}/bin/nixos-rebuild ${rebuildArg} --target-host ${target} --use-remote-sudo "$@"
                '';
                "${name}-cleanup" = pkgs.writeScriptBin "${name}-cleanup" ''
                  #!${pkgs.runtimeShell} -ex
                  ssh ${target} "nix-collect-garbage -d --verbose && nix-store --optimise --verbose"
                '';
                "${name}-lxc-template" = pkgs.writeScriptBin "${name}-lxc-template" ''
                  #!${pkgs.runtimeShell} -ex
                  ${nixos-generators.packages.${pkgs.system}.nixos-generate}/bin/nixos-generate -f proxmox-lxc --flake ${self}#${name}
                '';
              })
            { }
            (builtins.attrNames self.nixosConfigurations)
        )
        self.legacyPackages;

      nixosConfigurations = {
        fdroid-news = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            inputs.fdroid-news.nixosModules.fdroid-news
            sops-nix.nixosModules.sops
            ./hosts/fdroid-news
            ./general/lxc-container.nix
            ./general/users.nix
          ];
        };

        gotosocial = nixpkgs-unstable.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            ./general/lxc-container.nix
            ./general/users.nix
            ./hosts/gotosocial
            sops-nix.nixosModules.sops
            { nixpkgs.overlays = [ self.overlay ]; }
          ];
        };

        mcserver = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            ./general/lxc-container.nix
            ./general/users.nix
            ./hosts/mcserver
            { nixpkgs.overlays = [ self.overlay ]; }
          ];
        };

        paperless = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            ./general/lxc-container.nix
            ./general/users.nix
            ./hosts/paperless
          ];
        };

        tgtg = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            ./general/lxc-container.nix
            ./general/users.nix
            ./hosts/tgtg
            self.nixosModules.tgtg
            sops-nix.nixosModules.sops
            { nixpkgs.overlays = [ self.overlay ]; }
          ];
        };

        minecraft = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            ./general/lxc-container.nix
            ./general/users.nix
            ./hosts/minecraft
            nix-minecraft.nixosModules.minecraft-servers
          ];
          specialArgs = inputs;
        };

        jellyfin = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            ./general/lxc-container.nix
            ./general/users.nix
            ./hosts/jellyfin
          ];
        };

        assange = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            sops-nix.nixosModules.sops
            ./general/general.nix
            ./hosts/assange
          ];
          specialArgs = { inherit inputs; };
        };
      };
    };
}
