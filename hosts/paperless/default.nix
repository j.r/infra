{ config, pkgs, lib, ... }:

{
  networking = {
    hostName = "paperless";
    firewall.allowedTCPPorts = [ 28981 ];
  };

  services.paperless = {
    enable = true;
    address = "10.42.6.22";
    settings = {
      PAPERLESS_OCR_LANGUAGE = "deu+eng";
      PAPERLESS_TASK_WORKERS = 2;
      PAPERLESS_TIME_ZONE = "Europe/Berlin";
      PAPERLESS_OCR_USER_ARGS = "{\"invalidate_digital_signatures\": true}";
    };
  };

  system.stateVersion = "22.05";
}
