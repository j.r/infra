{ pkgs, lib, ... }:
{
  networking.hostName = "mcserver";
  networking.firewall.enable = false;

  services.minecraft-server = {
    enable = true;
    eula = true;
    declarative = true;
    whitelist = {
      jugendhacker = "50725367-d90d-497e-b6ae-3100986891b3";
    };
    jvmOpts = "-Xmx2048M -Xms1024M";
    serverProperties = {
      enforce-whitelist = true;
      max-players = 5;
      motd = "Testserver";
      snooper-enabled = false;
      spawn-protection = 0;
      view-distance = 8;
      white-list = true;
    };

    package =
      let
        version = "1.19";
        url = "https://launcher.mojang.com/v1/objects/e00c4052dac1d59a1188b2aa9d5a87113aaf1122/server.jar";
        sha256 = "1cnjrqr2vn8gppd1y1lcdrc46fd7m1b3zl28zpbw72fgy1bd1vyy";
      in
      (pkgs.minecraft-server.overrideAttrs (old: rec {
        name = "minecraft-server-${version}";
        inherit version;

        src = pkgs.fetchurl {
          inherit url sha256;
        };
      }));
  };

  environment.noXlibs = lib.mkForce false;
  nixpkgs.config.allowUnfree = true;

  system.stateVersion = "22.05";
}
