{ pkgs, lib, config, inputs, ... }:
let
  backupFilesScript = pkgs.writeShellApplication {
    name = "list-backup-files.sh";
    runtimeInputs = config.environment.systemPackages;
    text = ''
      set -eo pipefile
      gotosocial-admin media list-attachments --local-only --log-level fatal
      gotosocial-admin media list-emojis --local-only --log-level fatal
    '';
  };
in
{
  networking = {
    hostName = "gotosocial";
    hosts = {
      "65.108.48.233" = [ "queer.af" ];
    };
    firewall.enable = false;
    firewall.allowedTCPPorts = [ 8080 ];
  };

  services.gotosocial = {
    enable = true;
    settings = {
      log-level = "warn";
      host = "social.jugendhacker.de";
      bind-address = "0.0.0.0";
      trusted-proxies = [
        "10.42.0.1"
      ];

      instance-expose-peers = true;
      instance-expose-suspended = true;
      http-client = {
        timeout = "30s";
      };
      smtp-host = "mail.jugendhacker.de";
      smtp-port = 587;
      smtp-username = "social@jugendhacker.de";
      smtp-from = "social@jugendhacker.de";
      media-remote-max-size = "100MiB";
    };
    environmentFile = config.sops.secrets.envfile.path;
  };

  services.borgbackup.jobs = {
    gotosocial-sqlite = {
      dumpCommand = pkgs.writeScript "backup-sqlite.sh" ''
        ${pkgs.sqlite}/bin/sqlite3 /var/lib/gotosocial/database.sqlite ".dump"
      '';
      doInit = true;
      extraCreateArgs = "--stdin-name database-dump.sql";
      repo = "u388855-sub1@u388855.your-storagebox.de:repo/";
      encryption = {
        mode = "repokey-blake2";
        passCommand = "cat ${config.sops.secrets.backupkey.path}";
      };
      environment = {
        BORG_RSH = "ssh -p23 -i /run/keys/id_ed25519_storagebox";
      };
      compression = "auto,lzma";
      startAt = "03:00";
      prune.keep = {
        daily = 7;
        weekly = 4;
        monthly = -1;
      };
    };
    gotosocial-media = {
      paths = [ "${backupFilesScript}/bin/list-backup-files.sh" ];
      extraCreateArgs = "--paths-from-command";
      repo = "u388855-sub1@u388855.your-storagebox.de:repo/";
      encryption = {
        mode = "repokey-blake2";
        passCommand = "cat ${config.sops.secrets.backupkey.path}";
      };
      environment = {
        BORG_RSH = "ssh -p23 -i /run/keys/id_ed25519_storagebox";
      };
      compression = "auto,lzma";
      startAt = "04:00";
      prune.keep = {
        daily = 7;
        weekly = 4;
        monthly = -1;
      };
    };
  };

  sops.secrets = {
    backupkey = {
      sopsFile = ../../secrets/gotosocial/backupkey;
      format = "binary";
    };
    ssh-key = {
      sopsFile = ../../secrets/gotosocial/id_ed25519_storagebox;
      format = "binary";
      path = "/run/keys/id_ed25519_storagebox";
    };
    envfile = {
      sopsFile = ../../secrets/gotosocial/gotosocial.env;
      owner = "gotosocial";
      format = "dotenv";
    };
  };

  system.stateVersion = "22.05";
}
