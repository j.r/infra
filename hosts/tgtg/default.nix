{ config, ... }:

{
  networking = {
    hostName = "tgtg";
  };

  services.tgtg = {
    enable = true;
    username = "togoodtogo@jugendhacker.de";
    settings = {
      TELEGRAM = {
        Enabled = true;
      };
    };
    environmentFile = config.sops.secrets.envfile.path;
  };

  sops.secrets = {
    envfile = {
      sopsFile = ../../secrets/tgtg/tgtg.env;
      format = "dotenv";
      owner = "tgtg";
    };
  };

  system.stateVersion = "23.05";
}
