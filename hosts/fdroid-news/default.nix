{ fdroid-news, config, pkgs, lib, ... }:

{
  networking = {
    hostName = "fdroid-news";
  };

  users.users.fdroid-news = {
    createHome = true;
    isNormalUser = true;
    group = "fdroid-news";
  };

  users.groups.fdroid-news = { };

  services.fdroid-news = {
    enable = true;
    user = "fdroid-news";
    group = "fdroid-news";
    username = "feedbot@jugendhacker.de";
    host = "jugendhacker.de:5223";
    passwordFile = config.sops.secrets.xmpp-password.path;
    muc = "android@conference.anoxinon.me";
    nick = "fdroid-news";
    repos = [
      "https://f-droid.org/repo"
      "https://apt.izzysoft.de/fdroid/repo"
      "https://molly.im/fdroid/foss/fdroid/repo"
      "https://guardianproject.info/fdroid/repo"
    ];
    debugMode = true;
  };

  environment.systemPackages = with pkgs; [
    sqlite
  ];

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    secrets.xmpp-password = {
      sopsFile = ../../secrets/fdroid-news/xmpp-login.yaml;
      restartUnits = [ "fdroid-news.service" ];
      owner = "fdroid-news";
      group = "fdroid-news";
    };
  };

  system.stateVersion = "22.05";
}
