{ config, inputs, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";

  fileSystems = {
    "/".options = [ "compress=zstd" ];
    "/home".options = [ "compress=zstd" ];
    "/nix".options = [ "compress=zstd" "noatime" ];
  };

  networking = {
    hostName = "assange";
    useDHCP = false;
    firewall = {
      allowedTCPPorts = [ 22 9001 9030 ];
      allowedUDPPorts = [ config.networking.wireguard.interfaces.wg0.listenPort ];
      interfaces = {
        wg0 = {
          allowedTCPPorts = [
            config.services.grafana.settings.server.http_port
            config.services.prometheus.port
          ];
        };
      };
    };
    interfaces.ens3 = {
      useDHCP = false;
      ipv4.addresses = [{
        address = with import ../../hosts.nix; assange.ipv4;
        prefixLength = 22;
      }];
      ipv6.addresses = [{
        address = with import ../../hosts.nix; assange.ipv6;
        prefixLength = 64;
      }];
    };
    defaultGateway = "89.58.0.1";
    defaultGateway6 = {
      address = "fe80::1";
      interface = "ens3";
    };
    nameservers = [
      "2a03:4000:0:1::e1e6"
      "2a03:4000:8000::fce6"
      "46.38.225.230"
      "46.38.252.230"
    ];
    wireguard.interfaces = {
      wg0 = {
        ips = [ "10.42.3.1/24" ];
        listenPort = 51820;
        privateKeyFile = config.sops.secrets.wireguard-key.path;
        peers = [
          {
            publicKey = "jRtZQXhhpztu2L8rvxo8SKGTa/gpIcyX7MPH4neygiw=";
            allowedIPs = [
              "10.42.0.0/24"
              "10.42.8.0/22"
            ];
            endpoint = "snowden.jugendhacker.de:57378";
          }
          {
            publicKey = "eQBtRuCYGf4ImvTUD80nxdTY8m7/fwBzbgq9IZ4whlg=";
            allowedIPs = [
              "10.42.2.0/24"
              "10.42.4.0/22"
              "192.168.89.0/24"
            ];
          }
        ];
      };
    };
  };

  console = {
    keyMap = "de";
  };

  users.users.julian = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = with import ../../general/ssh-keys.nix; [ jr ];
  };

  security.sudo.wheelNeedsPassword = false;

  services = {
    beesd.filesystems.nix = {
      spec = "/nix";
      hashTableSizeMB = 128;
    };
    tor = {
      package = inputs.nixpkgs-unstable.legacyPackages.${pkgs.system}.tor;
      enable = true;
      relay = {
        enable = true;
        role = "relay";
      };
      settings = {
        ORPort = 9001;
        Nickname = "jugendhackerassange";
        RelayBandwidthRate = "2560KB";
        RelayBandwidthBurst = "3MB";
        MaxAdvertisedBandwidth = "3MB";
        AccountingMax = "15360 GB";
        AccountingStart = "month 1 00:00";
        ContactInfo = "j.r <tor AT jugendhacker dot de>";
        DirPort = 9030;
        ControlPort = 9051;
        CookieAuthentication = true;
        MetricsPort = 8083;
        MetricsPortPolicy = "accept 127.0.0.1";
      };
    };
    grafana = {
      enable = true;
      settings.server = {
        http_port = 8080;
        http_addr = "10.42.3.1";
      };
    };
    prometheus = {
      enable = true;
      port = 8081;
      exporters.node = {
        enable = true;
        enabledCollectors = [
          "systemd"
          "btrfs"
          "cpu"
        ];
        port = 8082;
      };
      scrapeConfigs = [
        {
          job_name = "assange";
          scrape_interval = "10s";
          static_configs = [{
            targets = [
              "127.0.0.1:${toString config.services.prometheus.exporters.node.port}"
              "127.0.0.1:${toString config.services.tor.settings.MetricsPort}"
            ];
          }];
        }
      ];
    };
    fail2ban.enable = true;
    openssh = {
      enable = true;
      settings = {
        LogLevel = "VERBOSE";
        PasswordAuthentication = false;
        KbdInteractiveAuthentication = false;
        PermitRootLogin = "no";
      };
    };
    journald.extraConfig = "SystemMaxUse=100M";
  };

  environment.systemPackages = with pkgs; [
    nyx
  ];

  sops = {
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    secrets.wireguard-key = {
      format = "binary";
      sopsFile = ../../secrets/assange/assange.key;
    };
  };

  nix.settings.trusted-users = [
    "root"
    "julian"
  ];

  system.stateVersion = "22.05";
}
