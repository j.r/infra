{ lib, nix-minecraft, pkgs, ... }:

let
  pufferfish = nix-minecraft.packages.${pkgs.system}.paper-server.overrideAttrs (oldAttrs: {
    src = pkgs.fetchurl {
      url = "https://ci.pufferfish.host/job/Pufferfish-1.21/21/artifact/build/libs/pufferfish-paperclip-1.21.1-R0.1-SNAPSHOT-mojmap.jar";
      hash = "sha256-xdtOjRK97f7jvXob0lMK+YndMtTDkCbaWHX3z4jc3sk=";
    };
  });
in
{
  networking = {
    hostName = "minecraft";
    firewall.enable = false;
  };

  services.minecraft-servers = {
    enable = true;
    eula = true;
    servers.meissen-crew = {
      package = nix-minecraft.packages.${pkgs.system}.paper-server;
      enable = true;
      managementSystem = {
        tmux.enable = false;
        systemd-socket.enable = true;
      };
      whitelist = {
        jugendhacker = "50725367-d90d-497e-b6ae-3100986891b3";
        Chapman1411 = "a2a5f8f6-66d4-40c8-9710-22968168c6ae";
        Albi1707 = "c56051e0-ad02-4345-aac3-0186d6e4cdc8";
        Belge = "32e54f7c-de3e-4f8e-98ab-a9fbd939b0bf";
        ibims_BB = "03b8b09c-68d4-466c-967d-8f6b3134a5fe";
        Morlie = "27d32ec2-655f-46c5-9511-43a230903482";
        Feuerschatte = "21fb094c-ed33-4894-826e-eb1f55acebc2";
        Yama7000 = "c3c1f978-31e0-4744-92be-47a6d14e172f";
        HierBinIch = "a2b18429-63f0-4360-9e57-f835491042b5";
        Reltalis = "b8c1aeb7-fb06-4b89-a097-65f0dcac8ec5";
        Drumeleon = "993c6b95-3d4f-48c9-a714-937aba56b534";
        JustJery = "0a570733-6d19-4a3f-81a8-b00061c67c27";
        W4hr = "f5dbd06e-0009-484a-b237-0af579e30d83";
        lachsfabrik94 = "fd3296b4-e13d-4bd5-b40d-d0138e70b4b4";
      };
      serverProperties = {
        white-list = true;
        server-port = 25567;
        view-distance = 20;
        difficulty = "hard";
      };
      jvmOpts = "-Xmx5G -Xms1G -XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -Dusing.aikars.flags=https://mcflags.emc.gs -Daikars.new.flags=true --add-modules=jdk.incubator.vector";
      symlinks = {
        "plugins/viaversion.jar" = pkgs.fetchurl {
          url = "https://hangarcdn.papermc.io/plugins/ViaVersion/ViaVersion/versions/5.2.1/PAPER/ViaVersion-5.2.1.jar";
          hash = "sha256-yaWqtqxikpaiwdeyfANzu6fp3suSF8ePmJXs9dN4H8g=";
        };
      };

      files = {
        "config/paper-global.yml" = (pkgs.formats.yaml { }).generate "paper-global.yaml" {
          unsupported-settings = {
            allow-headless-pistons = true;
            allow-permanent-block-break-exploits = true;
            allow-piston-duplication = true;
          };
        };
      };
    };
  };

  nixpkgs.config.allowUnfree = true;

  system.stateVersion = "23.05";
}
