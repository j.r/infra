{ pkgs, ... }:

{
  networking = {
    hostName = "jellyfin";
    firewall.enable = false;
  };

  services.jellyfin = {
    enable = true;
  };

  environment = {
    systemPackages = with pkgs; [
      rsync
    ];
  };

  system.stateVersion = "23.05";
}
