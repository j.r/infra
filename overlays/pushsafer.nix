{
  python3Packages,
  fetchPypi,
  ...
}:

python3Packages.buildPythonPackage rec {
  pname = "python-pushsafer";
  version = "1.1";
  format = "setuptools";

  src = fetchPypi {
    inherit pname version;
    hash = "sha256-oPkukRjBoNUPkJaNvPxnyA0DT8eHFVuJPP9zFvCCeGQ=";
  };
}
