{
  python3Packages,
  fetchFromGitHub,
  python-pushsafer,
  ...
}:

python3Packages.buildPythonApplication rec {
  pname = "tgtg";
  version = "1.22.2";

  pyproject = true;

  patches = [ ./0001-Fix-discord.py-dependency.patch ];

  build-system = with python3Packages; [
    poetry-core
  ];

  dependencies = with python3Packages; [
    requests
    pycron
    progress
    apprise
    colorlog
    cron-descriptor
    discordpy
    googlemaps
    humanize
    prometheus-client
    python-telegram-bot
    python-pushsafer
    packaging
    babel
  ];

  src = fetchFromGitHub {
    owner = "Der-Henning";
    repo = "tgtg";
    tag = "v${version}";
    hash = "sha256-ckeP6nTA4TuZafmE3woxov/uCluRWZQu3K8ocbnHtAo=";
  };
}
