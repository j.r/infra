{ poetry2nix, fetchFromSourcehut, lib, python310, nodejs-16_x, python310Packages, buildNpmPackage, nodePackages, tdlib, substituteAll, ... }:

poetry2nix.mkPoetryApplication rec {
  pname = "slidge";
  version = "0.1.0rc1";

  python = python310;

  projectDir = src;

  extras = [ "signal" "facebook" "telegram" "skype" "mattermost" "steam" "discord" ];

  overrides = poetry2nix.overrides.withDefaults (self: super: {
    aiosignald = super.aiosignald.overridePythonAttrs (old: {
      buildInputs = old.buildInputs ++ [ self.poetry ];
    });
    iniconfig = super.iniconfig.overridePythonAttrs (old: {
      buildInputs = old.buildInputs ++ [ self.hatchling self.hatch-vcs ];
    });
    skpy = super.skpy.overridePythonAttrs (old: {
      buildInputs = old.buildInputs ++ [ self.setuptools ];
    });
    mattermost-api-reference-client = super.mattermost-api-reference-client.overridePythonAttrs (old: {
      buildInputs = old.buildInputs ++ [ self.poetry ];
    });
    discord-py-self = super.discord-py-self.overridePythonAttrs (old: {
      buildInputs = old.buildInputs ++ [ self.setuptools ];
    });
    pickle-secure = super.pickle-secure.overridePythonAttrs (old: {
      buildInputs = old.buildInputs ++ [ self.poetry ];
    });
    sphinxcontrib-applehelp = super.sphinxcontrib-applehelp.overridePythonAttrs (old: {
      buildInputs = old.buildInputs ++ [ self.flit ];
    });
    sphinx = super.sphinx.overridePythonAttrs (old: {
      buildInputs = old.buildInputs ++ [ self.flit ];
    });
    sphinx-argparse = super.sphinx-argparse.overridePythonAttrs (old: {
      buildInputs = old.buildInputs ++ [ self.poetry ];
    });
    sphinx-autoapi = super.sphinx-autoapi.overridePythonAttrs (old: {
      buildInputs = old.buildInputs ++ [ self.setuptools ];
    });
    mautrix-facebook = super.mautrix-facebook.overridePythonAttrs (old: {
      buildInputs = old.buildInputs ++ [ self.setuptools ];
    });
    types-emoji = super.types-emoji.overridePythonAttrs (old: {
      buildInputs = old.buildInputs ++ [ self.setuptools ];
    });
    aiotdlib = super.aiotdlib.overridePythonAttrs (old: {
      buildInputs = old.buildInputs ++ [ self.poetry ];
      nativeBuildInputs = old.nativeBuildInputs ++ [ tdlib ];
      patches = [
        (substituteAll {
          src = ./0001-tdjson-path.patch;
          tdjson = "${tdlib}/lib/libtdjson.so";
        })
      ];
    });
    furo = super.furo.overridePythonAttrs (old: rec{
      format = "other";
      node-deps = buildNpmPackage {
        src = old.src;
        pname = "${old.pname}-node-modules";
        version = "${old.version}";
        patches = [
          ./0001-Fix-lockfile.patch
        ];
        npmDepsHash = "sha256-9y1MESv6PeZ3y7iv8Q22L6AJnnBjxPWiqmpao9dRGWo=";
        dontNpmBuild = true;
        dontNpmInstall = true;
        installPhase = ''
          mkdir -p $out
          cp -R node_modules $out/
        '';
      };
      postPatch = ''
        sed -i -e 's/node-version = "16.15.1"/node-version = "16.18.1"/g' pyproject.toml
      '';
      buildPhase = ''
        export STB_USE_SYSTEM_NODE=1
        mkdir -p .nodeenv
        cp -r ${node-deps}/node_modules .
        python -m build --no-isolation --wheel
      '';
      nativeBuildInputs = [ nodejs-16_x ];
      buildInputs = old.buildInputs ++ [ self.build self.sphinx-theme-builder python310Packages.pipInstallHook ];
    });
    rich = super.rich.overridePythonAttrs (old: {
      propagatedBuildInputs = [ self.commonmark self.pygments ];
    });
    sphinx-theme-builder = super.buildPythonPackage rec{
      pname = "sphinx-theme-builder";
      version = "0.2.0b1";

      src = super.fetchPypi {
        inherit pname version;
        sha256 = "sha256-6btKCoUWurh2m53fADtw5YeGEREzGesf22kK+Eo6WV8=";
      };

      propagatedBuildInputs = [ self.rich self.packaging self.setuptools self.pyproject-metadata python310Packages.nodeenv ];

      format = "flit";

      doCheck = false;
    };
  });

  src = fetchFromSourcehut rec{
    inherit pname version;
    owner = "~nicoco";
    repo = "${pname}";
    rev = "v${version}";
    sha256 = "sha256-snHMUaoqgSXkAG7+Km0kzSDYjKXY7LlkFgb0ZB1oCUc=";
    vc = "git";
    domain = "sr.ht";
    fetchSubmodules = false;
  };
}
