{ lib, pkgs, stdenv, fetchurl, bash, jre, git, maven }:
let
  spigot_rev = "1.19";
in
stdenv.mkDerivation {
  pname = "spigotmc";
  version = "${spigot_rev}";

  srcs = [
    (fetchurl {
      url = "https://hub.spigotmc.org/jenkins/job/BuildTools/147/artifact/target/BuildTools.jar";
      sha256 = "1y72jqzjgw5xzkbaxkzlbcv370lmbi11qifsknzq1s5wv53l5f6r";
    })
    /* (fetchgit {
      url = "https://hub.spigotmc.org/stash/scm/spigot/bukkit.git";
      rev = "4b08dbc5ce3ddb031df2cff7be350842670f1929";
      sha256 = "0000000000000000000000000000000000000000000000000000";
      deepClone = true;
      })
      (fetchgit {
      url = "https://hub.spigotmc.org/stash/scm/spigot/builddata.git";
      rev = "e6ebde42e39100b18ca0265596b04f557b2b27cc";
      sha256 = "0000000000000000000000000000000000000000000000000000";
      deepClone = true;
      })
      (fetchgit {
      url = "https://hub.spigotmc.org/stash/scm/spigot/craftbukkit.git";
      rev = "50ef122ef8f56e7e36860e94e6e0d986165fb30e";
      sha256 = "0000000000000000000000000000000000000000000000000000";
      deepClone = true;
      })
      (fetchgit {
      url = "https://hub.spigotmc.org/stash/scm/spigot/bukkit.git";
      rev = "refs/heads/master";
      sha256 = "0000000000000000000000000000000000000000000000000000";
      deepClone = true;
      })*/
  ];

  nativeBuildInputs = [ git maven ];
  buildInputs = [ jre ];

  unpackPhase = ''
    for srcFile in $srcs; do
      cp $srcFile $(stripHash $srcFile)
    done
  '';

  dontConfigure = true;

  buildPhase = ''
    export HOME=$TMP
    ${jre}/bin/java -jar BuildTools.jar -rev ${spigot_rev}
    cat > minecraft-server << EOF
    #!${bash}/bin/sh
    exec ${jre}/bin/java \$@ -jar $out/share/spigotmc/spigot.jar nogui
  '';

  installPhase = ''
    install -Dm444 spigot-${spigot_rev}.jar $out/share/spigotmc/spigot.jar
    install -Dm555 -t $out/bin minecraft-server
  '';
}
