self: super:

{
  spigotmc = super.callPackage ./spigotmc.nix { };
  tdlib_1_8_4 = super.tdlib.overrideAttrs (_: {
    version = "1.8.4";
    src = super.fetchFromGitHub {
      owner = "tdlib";
      repo = "td";
      rev = "6d485aa0b672ca4b5eccf05d6e5538c6988f12ef";
      sha256 = "sha256-UENzjmSH+yrNFJgm0wUr8cBKGGPeOo0WvAP12SRvzvY=";
    };
  });
  slidge = super.callPackage ./slidge.nix { nodejs = super.nodejs-16_x; tdlib = self.tdlib_1_8_4; };
  tgtg = super.callPackage ./tgtg.nix { };
  python-pushsafer = super.callPackage ./pushsafer.nix { };
}
