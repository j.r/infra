{ config, pkgs, lib, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/profiles/minimal.nix")
    (modulesPath + "/virtualisation/proxmox-lxc.nix")
    ./general.nix
  ];

  # nix.settings.sandbox = false;
  # networking.useDHCP = false;

  /*networking.interfaces.eth0 = {
    useDHCP = true;
    mtu = 1420;
  };*/

  proxmoxLXC = {
    manageNetwork = true;
    manageHostName = true;
    privileged = true;
  };
}
