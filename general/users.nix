{
  users.users = {
    root = {
      openssh.authorizedKeys.keys = with import ./ssh-keys.nix; [ jr nixos-vm ];
    };
  };
}
