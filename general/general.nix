{ pkgs, ... }:

{
  services.openssh.enable = true;
  environment = {
    systemPackages = with pkgs; [
      curl
      wget
      htop
      tmux
      vim
      git
      rsync
    ];
    # noXlibs = true;
  };

  time.timeZone = "Europe/Berlin";

  nix = {
    package = pkgs.nixVersions.stable;
    extraOptions = "experimental-features = nix-command flakes";
    gc = {
      automatic = true;
      dates = "04:00";
      randomizedDelaySec = "60min";
      options = "--delete-older-than 7d";
    };
  };
}
