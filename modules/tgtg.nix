{
  config,
  pkgs,
  lib,
  ...
}:

let
  inherit (pkgs) tgtg;

  cfg = config.services.tgtg;
  settingsFormat = pkgs.formats.ini { };
  configFile = settingsFormat.generate "tgtg-config.ini" cfg.settings;
in
{
  options.services.tgtg = with lib; {
    enable = mkEnableOption "TGTG Scanner";
    username = mkOption {
      type = types.str;
    };
    settings = mkOption {
      type = types.submodule {
        freeformType = settingsFormat.type;
      };
    };
    environmentFile = lib.mkOption {
      type = lib.types.nullOr lib.types.path;
      default = null;
    };
  };

  config = lib.mkIf cfg.enable {
    services.tgtg.settings = {
      TGTG = {
        Username = cfg.username;
      };
    };

    users.groups.tgtg = {};
    users.users.tgtg = {
      group = "tgtg";
      isSystemUser = true;
    };

    environment.etc."tgtg/config.ini".source = configFile;

    systemd.services."tgtg-scanner" = {
      description = "TGTG Scanner";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      restartIfChanged = true;
      restartTriggers = [ configFile ];
      serviceConfig = {
        Type = "exec";
        EnvironmentFile = lib.mkIf (cfg.environmentFile != null) cfg.environmentFile;
        ExecStart = "${tgtg}/bin/scanner -c /etc/tgtg/config.ini";
        StateDirectory = "tgtg";
        WorkingDirectory = "/var/lib/tgtg";
        Restart = "always";

        User = "tgtg";
        Group = "tgtg";
        ProtectHome = true;
        ProtectSystem = "strict";
        NoNewPriviliges = true;
        PrivateTmp = true;
        PrivateDevices = true;
      };
    };
  };
}
