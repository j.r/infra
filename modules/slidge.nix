{ config, lib, pkgs, ... }:

let
  cfg = config.services.slidge;
  inherit (pkgs) slidge tdlib;
  gatewayOptions = with lib; {
    options = {
      legacy-module = mkOption {
        description = "Importable python gateway module";
        example = "slidge.plugins.signal";
        type = types.str;
      };
      secret = mkOption {
        description = "Gateway component secret for server connection";
        type = types.str;
      };
      jid = mkOption {
        description = "JID of gateway component";
        type = types.str;
      };
      port = mkOption {
        description = "Port for component connection";
        type = types.port;
        default = 5347;
      };
      server = mkOption {
        description = "Hostname of the XMPP server";
        type = types.str;
      };
      user-jid-validator = mkOption {
        type = types.str;
      };
    };
  };
in
{
  options.services.slidge = with lib; {
    enable = mkEnableOption "Slidge";
    gateways = mkOption {
      description = "List of slidge gateways to be configured";
      type = with types; listOf (submodule gatewayOptions);
    };
  };

  config = lib.mkIf cfg.enable {
    environment.etc = builtins.foldl'
      (configs: gatewayCfg:
        configs // {
          "slidge/${gatewayCfg.jid}.conf".text = lib.generators.toKeyValue { } gatewayCfg;
        }
      )
      { }
      cfg.gateways;
    systemd.services = lib.lists.foldl
      (services: gatewayCfg:
        services // {
          "slidge-${gatewayCfg.jid}" = {
            description = "Slidge gateway for ${gatewayCfg.jid}";
            wantedBy = [ "multi-user.target" ];
            after = [ "network.target" "network-online.target" ];
            wants = [ "network-online.target" ];
            restartIfChanged = true;
            serviceConfig = {
              Type = "exec";
              ExecStart = "${slidge}/bin/slidge -c /etc/slidge/${gatewayCfg.jid}.conf --home-dir /var/lib/slidge-${gatewayCfg.jid}";
              Restart = "on-failure";
              RestartSec = "5s";
              KillSignal = "SIGINT";
              StateDirectory = "slidge-${gatewayCfg.jid}";
              ConfigurationDirectory = "/etc/slidge";

              DynamicUser = true;
              ProtectHome = true;
              ProtectSystem = "strict";
              NoNewPriviliges = true;
              PrivateTmp = true;
              PrivateDevices = true;
              RestrictAddressFamilies = "AF_UNIX AF_INET AF_INET6";
              RestrictNamespaces = true;
              RestrictRealtime = true;
              DevicePolicy = "closed";
              ProtectControlGroups = true;
              ProtectKernelModules = true;
              ProtectKernelTunables = true;
              LockPersonality = true;
            };
          };
        }
      )
      { }
      cfg.gateways;
  };
}
