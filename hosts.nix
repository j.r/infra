{
  fdroid-news = {
    ipv4 = "10.42.6.15";
  };

  gotosocial = {
    ipv4 = "10.42.6.16";
  };

  mcserver = {
    ipv4 = "10.42.6.17";
  };

  paperless = {
    ipv4 = "10.42.6.22";
  };

  minecraft = {
    ipv4 = "10.42.6.35";
  };

  tgtg = {
    ipv4 = "10.42.6.38";
  };

  jellyfin = {
    ipv4 = "192.168.0.79";
  };

  assange = {
    user = "julian";
    ipv4 = "89.58.0.34";
    ipv6 = "2a03:4000:5d:fab::1";
  };
}
